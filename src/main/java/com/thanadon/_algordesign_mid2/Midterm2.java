/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon._algordesign_mid2;

import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Midterm2 {
    private static void sort_Subarray(List<Integer> arr){ //ฟังก์ชันสำหรับหา subarray ที่เลขเรียงลำดับจากน้อยไปมาก
        int maxLen = 1, len = 1, startIndex = 0; //maxLen สำหรับบันทึกค่าขนาดที่ยาวที่สุดของ subarray,   len สำหรับหา maxLen,   startIndex ตำแหน่งเริ่มต้นของ subarray
        for(int i = 0; i < arr.size()-1; i++){ //วนลูปหา startIndex, maxLen
            if(arr.get(i) < arr.get(i+1)){ //ถ้าเลขถัดไปมากกว่าเลขปัจจุบัน
                len++; //ให้ทำการเพิ่ม len ไปหนึ่ง
            }else{
                if(maxLen < len){ 
                    maxLen = len; //ให้เปลี่ยนค่าของ maxLen เป็นค่าของ len
                    startIndex = (i+1)-maxLen; //ให้ค่าของ startIndex เท่ากับค่าของตำแหน่งปัจจุบันบวกหนึ่งลบด้วย maxLen 
                                                                   //**สาเหตุที่ต้องบวกหนึ่งเพราะเราเทียบกับตำแหน่งถัดไป ต้องนำตำแหน่งถัดไปลบด้วย maxLen จึงจะได้ตำแหน่งเริ่มต้น
                }
                len = 1; // หลังจากได้ค่า maxLen ใหม่แล้ว ต้องทำการ reset ค่า len เพื่อนำไปหาขนาดของ subarray ถัดไป
            }
        }
        //กรณีที่เลขเรียงไปจนถึงตัวสุดท้ายของ arr จะทำให้ maxLen ยังไม่ถูกเช็ค จึงต้องมาดักข้างนอกอีกครั้ง
        if(maxLen < len){ 
            maxLen = len;
            startIndex = arr.size()-maxLen;
        }
        int endIndex = (maxLen-1)+startIndex; // นำ maxLen(-1) บวกกับ startIndex เพื่อให้ได้ค่าของ endIndex ของ subarray 
        
        List<Integer> subarray = new ArrayList(); //ประกาศอาร์เรย์สำหรับเก็บค่า subarray
        for(int i = startIndex; i <= endIndex; i++){ // วนลูปเริ่มจากตำแหน่งแรกของ subarray จนถึงตำแหน่งสุดท้ายของ subarray
            subarray.add(arr.get(i)); //นำ arr ตำแหน่ง i เก็บลงใน subarray
        }
        
        System.out.println("Output sort subarray: "+subarray); // แสดงผลลัพธ์ของ subarray
    }
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String str = kb.nextLine(); //รับค่าข้อความ
        List<String> arr = new ArrayList(Arrays.asList(str.split(" "))); // ตัดช่องว่างของข้อความและเก็บลงอาร์เรย์
        
        List<Integer> A = new ArrayList(); // ประกาศอาร์เรย์ A สำหรับเก็บตัวเข
        for(int i = 0; i < arr.size(); i++){ // วนลูปตามขนาดของ arr
            A.add(parseInt(arr.get(i))); // แปลงค่าข้อความเป็นจำนวนเต็มและเก็บลงไปใน A
        }
        
        System.out.println("Input array A: "+A); // แสดงผลลัพธ์ของ A
        sort_Subarray(A); // เรียกใช้งานฟังก์ชัน
    }
}
