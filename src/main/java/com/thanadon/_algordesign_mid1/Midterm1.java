/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon._algordesign_mid1;

import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Midterm1 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int c = kb.nextInt(); //รับค่าจำนวนเต็ม c
        String str = kb.nextLine(); //รับค่าข้อความ
        List<String> arr = new ArrayList(Arrays.asList(str.split(" "))); //ตัดส่วนที่เป็นช่องว่างเก็บใน list arr
        arr.remove(""); //ลบค่าที่เป็นช่องว่างออก
        
        List<Integer> A = new ArrayList(); //ประกาศ list A
        for(int i = 1; i <= arr.size(); i++){ //วนลูปเพิ่มข้อมูลตัวเลขเข้าไปใน A
            int num = parseInt(arr.get(i-1)); //แปลงข้อความเป็นเลข
            if(!(num >= 1 && num <= 5*arr.size())){ //เช็คว่าเลขถูกกับเงื่อนไขไหม
                System.out.println("Integer must in range 1 to 5*length of array");
                return;
            }
            A.add(num); //เพิ่มตัวเลขเข้าไปใน A
        }
        
        for(int i = 1; i <= A.size()-1; i++){ //วนลูปหาผลลัพธ์ขอโปรแกม
            for(int j = i+1; j <= A.size(); j++){
                if(c==A.get(i-1)+A.get(j-1)){ //เช็คว่าค่า c เท่ากับค่าของตัวเลขใน A บวกกันสองตัวหรือไม่
                    System.out.println(c+" = "+A.get(i-1)+"+"+A.get(j-1));
                    return;
                }
            }
        }
        
        System.out.println("Not found");
    }  
}
